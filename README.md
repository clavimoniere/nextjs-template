# Next.js on GitLab Pages

This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app). This app has been slightly modified to work with GitLab pages.

You can see this example app at <https://clavimoniere.gitlab.io/nextjs-template>

## Before you begin

In [`next.config.js`](./next.config.js?ref_type=heads#L4), set `GITLAB_PROJECT_NAME` to the name of your repository (default value is `nextjs-template`). This will ensure your page works correctly when deployed to GitLab Pages.

## Development

To run the development server:

```bash
npm run dev
# or
yarn dev
# or
pnpm dev
# or
bun dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `app/page.js`. The page auto-updates as you edit the file.

This project uses [`next/font`](https://nextjs.org/docs/basic-features/font-optimization) to automatically optimize and load Inter, a custom Google Font.

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

You can check out [the Next.js GitHub repository](https://github.com/vercel/next.js/) - your feedback and contributions are welcome!

## Deploy to GitLab pages

Your page will deploy to GitLab pages automatically when you push to the `main` branch. The configuration used to build and deploy your site to GitLab pages can be found in `.gitlab-ci.yml`.
