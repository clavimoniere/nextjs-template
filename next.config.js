/** @type {import('next').NextConfig} */

// Set this const to the name of your repo
const GITLAB_PROJECT_NAME = 'nextjs-template'

const nextConfig = {
    assetPrefix: process.env.NODE_ENV === 'production' ? `/${GITLAB_PROJECT_NAME}` : '',  
    output: 'export' 
}

module.exports = nextConfig
